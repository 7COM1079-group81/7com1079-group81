Allmeansnewdf$std_dev[1]  <- sd(Actionsdf $ 'Maximum Installs')
Allmeansnewdf$std_dev[2]  <-sd(Foofdf $ 'Maximum Installs')
Allmeansnewdf$std_dev[3]  <-sd(Racedf $ 'Maximum Installs')
Allmeansnewdf$std_dev[4]  <-sd(Shoppingdf $ 'Maximum Installs')
Allmeansnewdf$std_dev[5]  <-sd(Simulationsdf $ 'Maximum Installs')
Allmeansnewdf$std_dev[6]  <-sd(Strategydf $ 'Maximum Installs')
Allmeansnewdf$std_dev[7]  <-sd(Traveldf $ 'Maximum Installs')
Allmeansnewdf$std_dev[8]  <-sd(Worddf $ 'Maximum Installs')
#Creating subset Foodsub0 of FoodDf
Foodsub0<-Foofdf[1:4390,"Maximum Installs"]
Foodsub0$Category<-"Food & Drink"
Actionsub<-Actionsdf[1:4390,"Maximum Installs"]
Actionsub$Category<-"Action"
Foodsub0<-rbind(Foodsub0,Actionsub)
#Performing F-test and student independent two sample t-test
var.test(Foodsub0$'Maximum Installs'[Foodsub0$Category=="Food & Drink"],Foodsub0$'Maximum Installs'[ Foodsub0$Category=="Action"])

t.test(Foodsub0$'Maximum Installs'[Foodsub0$Category=="Food & Drink"],Foodsub0$'Maximum Installs'[Foodsub0$Category=="Action"],var.equal=TRUE)
#Creating subset Foodsub1 of FoodDf
Foodsub1<-Foofdf[4391:8780,"Maximum Installs"]
Foodsub1$Category<-"Food & Drink"
Foodsub1<-rbind(Foodsub0,Actionsub)
#Performing F-test and student independent two sample t-test
var.test(Foodsub1$'Maximum Installs'[Foodsub1$Category=="Food & Drink"],Foodsub1$'Maximum Installs'[ Foodsub1$Category=="Action"])

t.test(Foodsub1$'Maximum Installs'[Foodsub1$Category=="Food & Drink"],Foodsub1$'Maximum Installs'[Foodsub1$Category=="Action"],var.equal=TRUE)
#Creating subset Foodsub2 of FoodDf
Foodsub2<-Foofdf[8781:12259,"Maximum Installs"]
Foodsub2$Category<-"Food & Drink"
Foodsub2<-rbind(Foodsub2,Actionsub)
#Performing F-test and student independent two sample t-test
var.test(Foodsub2$'Maximum Installs'[Foodsub2$Category=="Food & Drink"],Foodsub2$'Maximum Installs'[ Foodsub2$Category=="Action"])

t.test(Foodsub2$'Maximum Installs'[Foodsub2$Category=="Food & Drink"],Foodsub2$'Maximum Installs'[Foodsub2$Category=="Action"],var.equal=TRUE)
#Creating subset Shopsub0 of Shoppinfdf
Shopsub0<-Shoppingdf[1:4970,"Maximum Installs"]
Shopsub0$Category<-"Shopping"
Simulationsub<-Simulationsdf[1:4970,"Maximum Installs"]
Simulationsub$Category<-"Simulation"
Shopsub0<-rbind(Shopsub0,Simulationsub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub0$'Maximum Installs'[Shopsub0$Category=="Shopping"],Shopsub0$'Maximum Installs'[ Shopsub0$Category=="Simulation"])

t.test(Shopsub0$'Maximum Installs'[Shopsub0$Category=="Shopping"],Shopsub0$'Maximum Installs'[Shopsub0$Category=="Simulation"],var.equal=TRUE)
#Creating subset Shopsub1 of Shoppinfdf
Shopsub1<-Shoppingdf[4971:9940,"Maximum Installs"]
Shopsub1$Category<-"Shopping"
Shopsub1<-rbind(Shopsub1,Simulationsub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub1$'Maximum Installs'[Shopsub1$Category=="Shopping"],Shopsub1$'Maximum Installs'[ Shopsub1$Category=="Simulation"])

t.test(Shopsub1$'Maximum Installs'[Shopsub1$Category=="Shopping"],Shopsub1$'Maximum Installs'[Shopsub1$Category=="Simulation"],var.equal=TRUE)
#Creating subset Shopsub2 of Shoppinfdf
Shopsub2<-Shoppingdf[9941:14156,"Maximum Installs"]
Shopsub2$Category<-"Shopping"
Shopsub2<-rbind(Shopsub2,Simulationsub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub2$'Maximum Installs'[Shopsub2$Category=="Shopping"],Shopsub2$'Maximum Installs'[ Shopsub2$Category=="Simulation"])

t.test(Shopsub2$'Maximum Installs'[Shopsub2$Category=="Shopping"],Shopsub2$'Maximum Installs'[Shopsub2$Category=="Simulation"],var.equal=TRUE)
#Creating subset Shopsub3 of Shoppinfdf
Shopsub3<-Shoppingdf[1:2653,"Maximum Installs"]
Shopsub3$Category<-"Shopping"
Strategysub<-Strategydf[1:2653,"Maximum Installs"]
Strategysub$Category<-"Strategy"
Shopsub3<-rbind(Shopsub3,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub3$'Maximum Installs'[Shopsub3$Category=="Shopping"],Shopsub3$'Maximum Installs'[ Shopsub3$Category=="Strategy"])

t.test(Shopsub3$'Maximum Installs'[Shopsub3$Category=="Shopping"],Shopsub3$'Maximum Installs'[Shopsub3$Category=="Strategy"],var.equal=TRUE)
#Creating subset Shopsub4 of Shoppinfdf
Shopsub4<-Shoppingdf[2654:5306,"Maximum Installs"]
Shopsub4$Category<-"Shopping"
Shopsub4<-rbind(Shopsub4,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub4$'Maximum Installs'[Shopsub4$Category=="Shopping"],Shopsub4$'Maximum Installs'[ Shopsub4$Category=="Strategy"])

t.test(Shopsub4$'Maximum Installs'[Shopsub4$Category=="Shopping"],Shopsub4$'Maximum Installs'[Shopsub4$Category=="Strategy"],var.equal=TRUE)
#Creating subset Shopsub5 of Shoppinfdf
Shopsub5<-Shoppingdf[5307:7959,"Maximum Installs"]
Shopsub5$Category<-"Shopping"
Shopsub5<-rbind(Shopsub5,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub5$'Maximum Installs'[Shopsub5$Category=="Shopping"],Shopsub5$'Maximum Installs'[ Shopsub5$Category=="Strategy"])

t.test(Shopsub5$'Maximum Installs'[Shopsub5$Category=="Shopping"],Shopsub5$'Maximum Installs'[Shopsub5$Category=="Strategy"],var.equal=TRUE)
#Creating subset Shopsub6 of Shoppinfdf
Shopsub6<-Shoppingdf[7960:10612,"Maximum Installs"]
Shopsub6$Category<-"Shopping"
Shopsub6<-rbind(Shopsub6,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub6$'Maximum Installs'[Shopsub6$Category=="Shopping"],Shopsub6$'Maximum Installs'[ Shopsub6$Category=="Strategy"])

t.test(Shopsub6$'Maximum Installs'[Shopsub6$Category=="Shopping"],Shopsub6$'Maximum Installs'[Shopsub6$Category=="Strategy"],var.equal=TRUE)
#Creating subset Shopsub7 of Shoppinfdf
Shopsub7<-Shoppingdf[10613:13265,"Maximum Installs"]
Shopsub7$Category<-"Shopping"
Shopsub7<-rbind(Shopsub7,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub7$'Maximum Installs'[Shopsub7$Category=="Shopping"],Shopsub7$'Maximum Installs'[ Shopsub7$Category=="Strategy"])

t.test(Shopsub7$'Maximum Installs'[Shopsub7$Category=="Shopping"],Shopsub7$'Maximum Installs'[Shopsub7$Category=="Strategy"],var.equal=TRUE)
#Creating subset Shopsub8 of Shoppinfdf
Shopsub8<-Shoppingdf[10613:13265,"Maximum Installs"]
Shopsub8$Category<-"Shopping"
Shopsub8<-rbind(Shopsub8,Strategysub)
#Performing F-test and student independent two sample t-test
var.test(Shopsub8$'Maximum Installs'[Shopsub8$Category=="Shopping"],Shopsub8$'Maximum Installs'[ Shopsub8$Category=="Strategy"])

t.test(Shopsub8$'Maximum Installs'[Shopsub8$Category=="Shopping"],Shopsub8$'Maximum Installs'[Shopsub8$Category=="Strategy"],var.equal=TRUE)
#Performing F-test and student independent two sample t-test
var.test(Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Action"],Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Simulation"],data="Googlenewdf",var.equal = TRUE)

t.test(Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Action"],Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Simulation"],data="Googlenewdf",var.equal = TRUE)
#Performing F-test and student independent two sample t-test
var.test(Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Shopping"],Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Food & Drink"],data="Googlenewdf")

t.test(Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Shopping"],Googlenewdf$`Maximum Installs`   [Googlenewdf$Category=="Food & Drink"],data="Googlenewdf",var.equal = TRUE)
